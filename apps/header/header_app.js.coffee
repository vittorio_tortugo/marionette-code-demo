@OlympiadsApp.module 'Olympiads.Header', (Header, App, Backbone, Marionette, $, _) ->
  class Header.Controller extends Marionette.Object
    initialize: ->
      new Stoege.Views.HeaderView
      @registration_view = new Stoege.Views.RegistrationDialog model: new Stoege.Models.RegistrationDialog

      Backbone.Router::after = =>
        params = Backbone.History.prototype.getQueryParameters(window.location.search)
        if params['registration']?
          @registration_view.open_registration(params)
        else
          @registration_view.destroy() if $('.registration_dialog').length

  App.on 'start', ->
    new Header.Controller
