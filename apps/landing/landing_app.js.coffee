@OlympiadsApp.module 'Olympiads.Landing', (Landing, App, Backbone, Marionette) ->
  class Landing.Controller extends Marionette.Object
    main: ->
      @main_view or= new Landing.MainView

  class Landing.Router extends Marionette.AppRouter
    controller: new Landing.Controller
    appRoutes:
      '': 'main'

  App.on 'start', ->
    new Landing.Router

