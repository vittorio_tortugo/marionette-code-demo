@OlympiadsApp.module 'Olympiads.Landing.TestGameQuestions', (TestGameQuestions, App, Backbone, Marionette) ->

  class TestGameQuestions.View extends Marionette.ItemView

    template: 'landing/test_game/templates/test_game'

    templateHelpers: ->
      total: @questions.length
      question: @questions[@current-1].question
      answers: @answers[@current-1].answer

    events:
      'click .answer': 'save_choice'

    save_choice: (e) ->
      @all_gods = @all_gods.concat(@gods[@current-1].gods[$(e.target).data('id')-1].name)
      @show_next()

    show_next: ->
      @current += 1
      @position += 100 / @count
      $('.runner').css 'left', @position + '%' if @position < 100
      if @current <= @count
        $('.current').html(@current)
        $('.question').html(@questions[@current-1].question)
        $('.answers_block').html('')
        for answer in @answers[@current-1].answer
          $('.answers_block').append "<a class='opacity_button answer' data-id='" + answer.id + "'>" + answer.option + "</a>"
      else
        sorted_name = @sort_by_frequency(@all_gods)
        @god_name = new Backbone.Model name: sorted_name[0]
        @trigger 'get:result', @god_name
        $('.answer').addClass 'last_answer'

    sort_by_frequency: (array) ->
      frequency = []
      array.forEach (value) ->
        frequency[value] = 0
      uniques = array.filter (value) ->
        ++frequency[value] is 1

      uniques.sort (a, b) ->
        frequency[b] - frequency[a]

    initialize: ->

      @gods = [
        {id: 1, gods: [
          {name: ['Зевс', 'Афина', 'Фид']}
          {name: ['Посейдон']}
          {name: ['Аполлон','Артемида','Афродита']}
          {name: ['Гестия','Деметра','Гера']}
          {name: ['Арес','Гефест']}
          ]}
        {id: 2, gods: [
          {name: ['Гестия', 'Деметра']}
          {name: ['Зевс', 'Аполлон','Гефест','Афродита']}
          {name: ['Афина']}
          {name: ['Посейдон','Арес','Артемида', 'Аид']}
          {name: ['Гера']}
          ]}
        {id: 3, gods: [
          {name: ['Аполлон']}
          {name: ['Деметра','Гестия']}
          {name: ['Афина', 'Гера']}
          {name: ['Посейдон','Арес','Афродита']}
          {name: ['Зевс','Артемида','Арес']}
          {name: ['Аид']}
          ]}
        {id: 4, gods: [
          {name: ['Афродита']}
          {name: ['Деметра']}
          {name: ['Зевс','Посейдон']}
          {name: ['Арес','Артемида', 'Афина']}
          {name: ['Гефест','Гестия', 'Аполлон']}
          {name: ['Аид']}
          {name: ['Гера']}
          ]}
        {id: 5, gods: [
          {name: ['Афина']}
          {name: ['Зевс', 'Посейдон']}
          {name: ['Арес','Гефест']}
          {name: ['Аполлон','Артемида']}
          {name: ['Деметра','Гестия','Гера']}
          {name: ['Афродита']}
          {name: ['Аид']}
          ]}
        {id: 6, gods: [
          {name: ['Гефест', 'Арес']}
          {name: ['Посейдон']}
          {name: ['Деметра','Гестия']}
          {name: ['Аполлон','Афродита']}
          {name: ['Афина']}
          {name: ['Зевс','Артемида']}
          {name: ['Аид']}
          {name: ['Гера']}
          ]}
        {id: 7, gods: [
          {name: ['Афродита', 'Артемида']}
          {name: ['Зевс', 'Афина']}
          {name: ['Аполлон','Посейдон']}
          {name: ['Деметра','Гестия', 'Гера']}
          {name: ['Гефест','Арес']}
          {name: ['Аид']}
          ]}
        {id: 8, gods: [
          {name: ['Гефест']}
          {name: ['Деметра']}
          {name: ['Посейдон','Арес']}
          {name: ['Гестия','Гера']}
          {name: ['Зевс','Афина']}
          {name: ['Аполлон','Афродита','Артемида']}
          {name: ['Аид']}
          ]}
        {id: 9, gods: [
          {name: ['Афродита', 'Аполлон']}
          {name: ['Арес', 'Аид']}
          {name: ['Зевс', 'Посейдон']}
          {name: ['Деметра','Гестия','Гера']}
          {name: ['Гефест']}
          {name: ['Афина']}
          {name: ['Артемида']}
          ]}
        {id: 10, gods: [
          {name: ['Зевс']}
          {name: ['Посейдон']}
          {name: ['Арес', 'Аид']}
          {name: ['Гефест', 'Гестия']}
          {name: ['Аполлон']}
          {name: ['Гера','Артемида', 'Деметра']}
          {name: ['Афина']}
          {name: ['Афродита']}
          ]}
        {id: 11, gods: [
          {name: ['Зевс']}
          {name: ['Посейдон']}
          {name: ['Арес']}
          {name: ['Гефест']}
          {name: ['Аполлон']}
          {name: ['Артемида']}
          {name: ['Афина']}
          {name: ['Афродита']}
          {name: ['Аид']}
          {name: ['Гестия']}
          {name: ['Гера','Деметра']}
          ]}
        {id: 12, gods: [
          {name: ['Арес', 'Афина']}
          {name: ['Зевс', 'Посейдон']}
          {name: ['Гефест']}
          {name: ['Аполлон','Афродита']}
          {name: ['Аид','Артемида']}
          {name: ['Деметра']}
          {name: ['Гестия','Гера']}
          ]}
        {id: 13, gods: [
          {name: ['Зевс', 'Арес','Аид']}
          {name: ['Гефест','Артемида','Деметра']}
          {name: ['Аполлон','Афина']}
          {name: ['Афродита','Гера','Гестия']}
          {name: ['Посейдон']}
          ]}
      ]

      @questions = [
        {id: 1, question: 'Жизнь — краткий миг между прошлым и будущим. За что тебя запомнят?'}
        {id: 2, question: 'Обманул бы ты своего партнера?'}
        {id: 3, question: 'Выбери вид спорта.'}
        {id: 4, question: 'Не все греческие боги носят белое. Одежду какого цвета ты любишь?'}
        {id: 5, question: 'Враг атакует! Твое секретное оружие?'}
        {id: 6, question: 'Какое животное могло бы быть твоим лучшим другом?'}
        {id: 7, question: 'Выдался свободный день. Как ты его проведешь?'}
        {id: 8, question: 'Какая черта описывает тебя лучше всего?'}
        {id: 9, question: 'Какое выражение характеризует твой взгляд на жизнь?'}
        {id: 10, question: 'Какой из этих школьных предметов нравится тебе больше всего?'}
        {id: 11, question: 'В какой стране ты хотел бы жить?'}
        {id: 12, question: 'В какой сфере ты хочешь построить карьеру в будущем?'}
        {id: 13, question: 'Твое любимое музыкальное направление...'}
      ]

      @answers = [
        {id: 1, answer: [{id: 1, option: 'Мудрость'},{id: 2, option: 'Сила'},{id: 3, option: 'Внешность'},{id: 4, option: 'Забота о других'},
        {id: 5, option: 'Креативность'}]}

        {id: 2, answer: [{id: 1, option: 'Я одиночка'},{id: 2, option: 'Зависит от ситуации'},{id: 3, option: 'Ни за что!'},
        {id: 4, option: 'Такое уже случалось'},{id: 5, option: 'Я лучше обману всех остальных!'}]}

        {id: 3, answer: [{id: 1, option: 'Футбол'},{id: 2, option: 'Люблю смотреть спортивные трансляции'},{id: 3, option: 'Фехтование'},
        {id: 4, option: 'Плавание'},{id: 5, option: 'Бокс/борьба'},{id: 6, option: 'Шахматы'}]}

        {id: 4, answer: [{id: 1, option: 'Розовый'},{id: 2, option: 'Желтый'},{id: 3, option: 'Пурпурный'},{id: 4, option: 'Красный'},
        {id: 5, option: 'Синий'},{id: 6, option: 'Черный'},{id: 7, option: 'Зеленый'}]}

        {id: 5, answer: [{id: 1, option: 'Супер-мозг'},{id: 2, option: 'Супер-сила'},{id: 3, option: 'Лошадь'},{id: 4, option: 'Лук'},
        {id: 5, option: 'Соблазнение'},{id: 5, option: 'Миротворческий талант'}, {id: 7, option: 'Мне не нужно оружие: я всегда побеждаю'}]}

        {id: 6, answer: [{id: 1, option: 'Тигр'},{id: 2, option: 'Дельфин'},{id: 3, option: 'Лебедь'},{id: 4, option: 'Павлин'},{id: 5, option: 'Сова'},
        {id: 6, option: 'Орел'}, {id: 7, option: 'Пёс'}, {id: 8, option: 'Кенгуру'}]}

        {id: 7, answer: [{id: 1, option: 'Займусь шоппингом'},{id: 2, option: 'С друзьями'},{id: 3, option: 'На пляже'},{id: 4, option: 'С семьёй'},
        {id: 5, option: 'Займусь спортом'},{id: 6, option: 'Уединюсь где-нибудь с книгой'}]}

        {id: 8, answer: [{id: 1, option: 'Мстительный'},{id: 2, option: 'Заботливый'},{id: 3, option: 'Агрессивный'},{id: 4, option: 'Покровительствующий'},
        {id: 5, option: 'Мудрый'},{id: 6, option: 'Красивый'}, {id: 7, option: 'Одинокий'}]}

        {id: 9, answer: [{id: 1, option: 'Красота спасет мир'},{id: 2, option: 'Быть или не быть? Вот в чём вопрос'},
        {id: 3, option: 'У природы нет плохой погоды'}, {id: 4, option: 'Нет места милее родного дома'},
        {id: 5, option: 'Дело мастера боится'},{id: 6, option: 'Врата мудрости не бывают закрытыми'},
        {id: 7, option: 'Нет ничего печальнее, чем зверь, запертый в клетке'}]}

        {id: 10, answer: [{id: 1, option: 'Физика'},{id: 2, option: 'География'},{id: 3, option: 'История'},{id: 4, option: 'Труд'},{id: 5, option: 'Музыка'},
        {id: 6, option: 'Биология'}, {id: 7, option: 'Обществознание'}, {id: 8, option: 'Литература'}]}

        {id: 11, answer: [{id: 1, option: 'Великобритания'},{id: 2, option: 'Австралия'},{id: 3, option: 'Германия'},
        {id: 4, option: 'Без разницы. Главное, в деревне, а не в городе'},{id: 5, option: 'США'},{id: 6, option: 'Швейцария'}, {id: 7, option: 'Италия'},
        {id: 8, option: 'Франция'},{id: 9, option: 'Атлантида'},{id: 10, option: 'Россия'},{id: 11, option: 'Китай'}]}

        {id: 12, answer: [{id: 1, option: 'Политика'},{id: 2, option: 'Энергетика и водоснабжение'},{id: 3, option: 'Архитектура и строительство'},
        {id: 4, option: 'Культура и искусство'},{id: 5, option: 'Медицина и ветеринария'},
        {id: 6, option: 'Сельское хозяйство'}, {id: 7, option: 'Я хочу посвятить себя семье'}]}

        {id: 13, answer: [{id: 1, option: 'Рок'},{id: 2, option: 'Фолк'},{id: 3, option: 'Классика'},{id:4, option: 'Поп'},{id: 5, option: 'Джаз'}]}
      ]

      @count = @questions.length
      @current = 1
      @position = 0
      @all_gods = []
