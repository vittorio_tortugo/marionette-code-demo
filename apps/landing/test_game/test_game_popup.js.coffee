@OlympiadsApp.module 'Olympiads.Landing.TestGamePopup', (TestGamePopup, App, Backbone, Marionette) ->

  class TestGamePopup.View extends Stoege.Views.Dialog

    template: JST["backbone/templates/landings/olymp_test_popup"]

    className: 'test_dialog'

    events:
      'click .close_button' : 'destroy'
      'click .scroll_to_test' : 'scroll_to_test'

    initialize: ->
      $(window).resize =>
        @set_dialog_position()

    scroll_to_test: ->
      $('.ui-dialog-content').dialog('close')
      $(window).scrollTo($('.test_wrapper'), 2500)

    open_dialog: ->
      @close_dialogs()
      @$el.html(@template)
      @open(925)
      @set_dialog_position()

    close_dialogs: ->
      $('.ui-dialog-content').dialog('close')

    destroy: ->
      @$el.empty()
      @$el.dialog('close')
