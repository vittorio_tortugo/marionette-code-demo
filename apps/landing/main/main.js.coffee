@OlympiadsApp.module 'Olympiads.Landing', (Landing, App, Backbone, Marionette) ->
  TestGameQuestion = App.module 'Olympiads.Landing.TestGameQuestions'
  TestGameResult = App.module 'Olympiads.Landing.TestGameResult'
  TestGamePopup = App.module 'Olympiads.Landing.TestGamePopup'

  class Landing.MainView extends Marionette.LayoutView
    el: '.main_content'

    template: false

    regions:
      test_game_region : '.test_wrapper'

    events:
      'click .scroll-button': 'scroll_to_info'
      'mousedown .marathon_wrapper' : 'start_dragging'
      'mousemove .marathon_wrapper' : 'move_layers'
      'mouseup .marathon_wrapper' : 'stop_dragging'
      'click .school_form_popup': 'open_form'
      'click .start_test_game': 'start_test_game'
      'click .presentation .registration': 'set_cookie_for_test_game'
      'click .fixed_menu .registration': 'set_cookie_for_test_game'
      'click .test_content .registration': 'start_test_game_after_load'

    initialize: ->
      @fixed_menu or= new Stoege.Views.FixedMenu
      @test_dialog_view = new TestGamePopup.View
      @social_share = new Backbone.Model text: 'Узнай, какой ты Олимпиец!'
      @social_share_view = new Stoege.Views.SocialShare(model: @social_share)
      @test_game_view = new TestGameQuestion.View
      $('.share').addClass('hidden')

      @scrollable = false
      @is_animate = false
      $(window).scroll (event) =>
        @start_animate()
        @set_triangle_height() if $(window).scrollTop() < 2000
        @scroll_to_test()

      @share_links = $('.share').detach()
      @init_share_links()
      @show_test_game_popup()
      if $.cookie('start_test_game_after_load')
        @start_test_game()
        $(window).scrollTo $('.test_wrapper')

    init_share_links: ->
      share_url = location.href.split('/').splice(0, 5).join('/')
      @$('.share div').ShareLink
        url: share_url
        title: @$('.social_buttons').data('data-course_full_title')
        text:  'Узнай, какой ты Олимпиец!'
        width: 640
        height: 480
      @$('.share .count').ShareCounter
        url: share_url
      @

    start_test_game_after_load: ->
      $.cookie('start_test_game_after_load', true, { path: '/o', expires: 7 })

    set_cookie_for_test_game: ->
      $.cookie('show_test_game_popup', true, { path: '/o', expires: 7 })

    show_test_game_popup: ->
      if $.cookie('show_test_game_popup') and $.cookie('is_registrated')
        @test_dialog_view.open_dialog()
        $('.test_dialog').first().parent().addClass('transparent_bg')
        $.removeCookie('show_test_game_popup')
        $.removeCookie('show_test_game_popup', { path: '/o' })

    show_result: (result)->
      @test_game_region.show @test_game_result_view
      $('.test_wrapper').addClass('test_wrapper_resize')
      $('.social_buttons').html(@share_links)
      $('.share').removeClass('hidden')

    start_test_game: ->
      $('.test_wrapper').addClass('test_wrapper_resize')
      @scrollable = false
      @scroll_to_test()
      $('.share').addClass('hidden')
      $('.test_wrapper').addClass('test_wrapper_clean_bg')
      $('.olympian_text').addClass('olympian_text_small')
      $('.olympian_text').removeClass('hidden')

      @test_game_view.on 'get:result', (result) =>
        @test_game_result_view = new TestGameResult.View(model: result)

        @test_game_result_view.on 'restart:game', =>
          @test_game_view = new TestGameQuestion.View
          @start_test_game()

        @show_result(result)
        $('.olympian_text').addClass('hidden')

      @test_game_region.show @test_game_view
      $('.test_content').addClass('test_content_small')

      $.removeCookie('start_test_game_after_load', { path: '/o' })

    open_form: ->
      school_form_view = new Stoege.Views.SchoolFormView model: new Stoege.Models.SchoolForm
      school_form_view.open_dialog()

    scroll_to_info: ->
      $(window).scrollTo($('.fixed_menu_wrapper+div'), 200)

    scroll_to_test: ->
      if ($('.test_wrapper').offset().top - $(window).scrollTop()) < 600 and not @scrollable
        $(window).scrollTo($('.test_wrapper'), 1000, interrupt: true)

        @scrollable = true
      else if ($('.test_wrapper').offset().top - $(window).scrollTop()) > 600
        @scrollable = false

    set_triangle_height: ->
      translate = $(window).scrollTop()/1.5
      translate_slow = $(window).scrollTop()/3
      $('.triangle_top_right').css top: -translate_slow, right: -translate_slow
      $('.triangle_top_left').css top: -translate_slow, left: -translate_slow
      $('.triangle_bottom_right').css bottom: -translate_slow, right: -translate_slow
      $('.triangle_bottom_left').css bottom: -translate_slow, left: -translate_slow
      $('.globe').css bottom: -translate, right: -translate
      $('.globe-shadow').css bottom: -translate, right: -translate
      $('.cup').css bottom: -translate, left: -translate
      $('.cup-shadow').css bottom: -translate, left: -translate

    start_animate: ->
      if $(window).scrollTop() > $(window).height() / 4
        $('.presentation_info').addClass 'animate_out'
        $('.slide-2').addClass 'animate_in'
        $('.hide_block').css 'opacity', 0
        @is_animate = true
      else
        $('.presentation_info').removeClass 'animate_out'
        $('.presentation_info').addClass 'animate_in' if @is_animate
        $('.slide-2').removeClass 'animate_in'
        $('.slide-2').addClass 'animate_out' if @is_animate
        $('.hide_block').css 'opacity', 1
      benefits_wrapper_offset = $('.benefits_wrapper').offset().top
      benefits_distance = (benefits_wrapper_offset - $(window).scrollTop())
      $('.benefits_wrapper .pic').children().addClass 'animate' if -400 < benefits_distance < 400

      prizes_wrapper_offset = $('.prizes_wrapper').offset().top
      benefits_distance = (prizes_wrapper_offset - $(window).scrollTop())
      $('.prizes_wrapper .prize_item div').addClass 'animate' if -400 < benefits_distance < 400

    move_layers: (event) ->
      if @dragging
        $('.scroll_horizontal').addClass 'animate'
        diff_x = event.pageX - @initial_x

        $('.marathon_wrapper').css 'background-position-x', diff_x/3
        $('.stadium_bg_2').css 'background-position-x', diff_x/2
        $('.stadium_bg_3').css 'background-position-x', diff_x/1.5
        $('.stadium').css 'background-position-x', diff_x
        $('.runners').css 'left', diff_x

    start_dragging: (event)->
      @dragging = true
      @initial_x = event.pageX

    stop_dragging: (event) ->
      @dragging = false
