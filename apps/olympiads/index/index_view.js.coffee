@OlympiadsApp.module 'Olympiads.Index', (Index, App, Backbone, Marionette) ->
  class Index.CardView extends Marionette.ItemView
    tagName: 'li'
    template: 'olympiads/index/templates/card'

  class Index.OlympiadsView extends Marionette.CompositeView
    childView: Index.CardView
    childViewContainer: 'ul'
    template: 'olympiads/index/templates/index'

    initialize: ({@validations, @collection}) ->
      return @

    serializeData: ->
      collection_valid_count:  @collection.get_count_of_valid_elements(@validations)

    filter: (child) ->
      @collection.try_validation_for(child, @validations)
