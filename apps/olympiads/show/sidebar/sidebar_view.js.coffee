@OlympiadsApp.module 'Olympiads.Show.Sidebar', (Sidebar, App, Backbone, Marionette) ->
  class Sidebar.View extends Marionette.ItemView
    events:
      'click .subscribe': 'fire_subscribe'
    template: 'olympiads/show/sidebar/templates/sidebar'
    modelEvents:
      'change:timer_end_data': 'render'
      'change:state': 'render'
      'change:user_subscribed': 'render'

    fire_subscribe: ->
      App.request 'subscribe_on_olympiad'
