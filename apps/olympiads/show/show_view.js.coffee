@OlympiadsApp.module 'Olympiads.Show', (Show, App, Backbone, Marionette) ->

  class Show.OlympiadsView extends Marionette.LayoutView
    el: '.course_page'
    events:
      'click .button_play_video': 'initialize_video_player'
    regions:
      sidebar_region: '.fxf_sidebar.state'

    initialize: ->
      App.reqres.setHandler 'subscribe_on_olympiad', @create_user_olympiad
      @render_sidebar()

    render_sidebar: ->
      @model.start_timer()
      @sidebar_view = new Show.Sidebar.View model: @model
      @sidebar_region.show @sidebar_view

    create_user_olympiad: =>
      user_olympiad = App.request 'get:user_olympiad'

      user_olympiad.save({olympiad_id: @model.get('id')}, {dataType: 'text'})
        .success =>
          @model.subscribe_user()
        .error ->
          notificationCollection.add_error 'Произошла ошибка'

    initialize_video_player: ->
      video_model = new Backbone.Model
      video_model.set 'url', 'https://www.youtube.com/embed/SWTMxaHUmKo'
      # TODO обстактный видео диалог и возможность автовоспроизведения
      new Stoege.Views.IosVideoDialogView(model: video_model).open_video()
