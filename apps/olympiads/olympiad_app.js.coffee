@OlympiadsApp.module 'Olympiads', (Olympiads, App, Backbone, Marionette) ->
  Filters = App.module 'Entities.Filters'

  class Olympiads.Controller extends Marionette.Object
    index: (params = {}) ->
      App.addRegions
        library_region: '#library'

      olympiads = App.request 'get:olympiads'

      @listenToOnce olympiads, 'sync', ->
        olympiads_view = new Olympiads.Index.OlympiadsView(collection: olympiads, validations: params)

        @initialize_filter(params)
        App.getRegion('library_region').show olympiads_view

    show: (olympiad_id) ->
      olympiad_model = App.request 'get:olympiad', olympiad_id
      @listenToOnce olympiad_model, 'sync', ->
        new Olympiads.Show.OlympiadsView model: olympiad_model

    initialize_filter: (params) ->
      @filter or= new Stoege.Views.LibraryFilter
        collection_seasons: new Filters.Seasons
        collection_disciplines: new Stoege.Collections.Disciplines
        collection_grades: new Stoege.Collections.Grades
        root_name: 'olympiads'
        params: params

  class Olympiads.Router extends Marionette.AppRouter
    controller: new Olympiads.Controller
    appRoutes:
      'olympiads': 'index'
      'olympiads/:id': 'show'

  App.on 'start', ->
    new Olympiads.Router
