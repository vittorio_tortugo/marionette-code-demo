@OlympiadsApp.module 'Olympiads.Rating', (Rating, App, Backbone, Marionette) ->
  
  class Rating.RatingResultsHiddenView extends Marionette.ItemView
    template: 'rating/templates/results_hidden'
    className: 'not_found olimpiads_close'
  
  class Rating.RatingNotFoundView extends Marionette.ItemView
    template: 'rating/templates/no_found'
    className: 'not_found'

  class Rating.RatingItemView extends Marionette.ItemView
    template: 'rating/templates/item'
    tagName: 'tr'
    className: -> (if @model.get('is_current') then 'current_user')

  class Rating.RatingCollectionView extends Marionette.CollectionView
    childView: Rating.RatingItemView
    template: false
    tagName: 'tbody'

    onShow: ->
      @$el = @$el.children()
      @$el.unwrap()
      @setElement(@$el)

  class Rating.ContentView extends Marionette.LayoutView
    template: 'rating/templates/content'

    events:
      'click .fxf_rating_list tfoot a' : 'load_more_ratings'

    regions:
      rating_body: 'tbody'

    collectionEvents:
      'sync': 'renderRating'    

    initialize: ({@validations, @active_season, @text}) ->  
      $(window).scroll =>
        @load_more_records()

    onRender: ->
      @renderRating()

    renderRating: ->
      @season_state = @active_season.get('state')

      rating_view = 
        if  @text is 'Школа'
          if @collection.length
            new Rating.RatingCollectionView(collection: @collection.fullCollection)
          else
            new Rating.RatingNotFoundView
        else
          if @season_state is 'results_opened'
            if @collection.length
              new Rating.RatingCollectionView(collection: @collection.fullCollection)
            else
              new Rating.RatingNotFoundView
          else
            new Rating.RatingResultsHiddenView      

      @getRegion('rating_body').show rating_view

    onShow: ->      
      @list_tbody_element = @$('tbody')
      @list_thead_element = @$('thead')
      @list_tfoot_element = @$('tfoot')

      @list_tfoot_element.toggleClass 'hidden', @collection.length < 10 or @season_state  isnt 'results_opened'
      @list_thead_element.toggleClass 'hidden', !@list_tbody_element.find('tr').text().length

      $('th.avatar').html(@text)
      $('th.rating').html(if @collection.first().get('season_state') is 'results_opened' then 'Баллы' else 'Ученики') if @collection.length

    load_more_records: ->
      $last_body_element = @list_tbody_element.children().last()
      return if not @collection.hasNextPage() or not @collection or @collection.length < 10
      return unless $last_body_element.length
      if $last_body_element.offset().top < ($(window).scrollTop() + $(window).height())
        @load_more_ratings()

    load_more_ratings: (e) ->
      e?.preventDefault()

      @collection.getNextPage
        data: @params
        beforeSend: =>
          @list_tfoot_element.show()
        complete: =>
          @list_tfoot_element.hide()
      @