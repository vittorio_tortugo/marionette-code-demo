@OlympiadsApp.module 'Olympiads.Rating', (Rating, App, Backbone, Marionette) ->

  Filters = App.module 'Entities.Filters'

  App.addInitializer ->
    new Rating.Router
      controller: new Rating.Controller

  class Rating.Router extends Marionette.AppRouter
    appRoutes:
      'school_ratings': 'show_school_ratings'
      'user_ratings': 'show_user_ratings'
      'olympiads/:id/school_ratings': 'show_olympiad_school_ratings'
      'olympiads/:id/user_ratings': 'show_olympiad_user_ratings'

  class Rating.Controller extends Marionette.Object

    show_school_ratings: (params = {}) ->
      @active_season = App.request 'get:active_season', @get_season_id(params)     

      @listenToOnce @active_season, 'sync', =>
        url_link = @get_url_link("#{window.olymp_root}/api/school_season_points_ratings", "#{window.olymp_root}/api/school_season_users_ratings")
        @show(params, url_link, "school_ratings", 'Школа')

    show_user_ratings: (params = {}) ->
      @active_season = App.request 'get:active_season', @get_season_id(params)    
      @listenToOnce @active_season, 'sync', =>  
        @show(params, "#{window.olymp_root}/api/user_season_ratings", "user_ratings", 'Ученик')

    show_olympiad_school_ratings: (id, params = {}) ->
      @active_season = App.request 'get:active_season', @get_season_id(params)      
      @listenToOnce @active_season, 'sync', =>
        params['ratingable_id'] = id
        url_link = @get_url_link("#{window.olymp_root}/api/school_olympiad_points_ratings", "#{window.olymp_root}/api/school_olympiad_users_ratings")
        @show(params, url_link, "olympiads/#{id}/school_ratings", 'Школа')

    show_olympiad_user_ratings: (id, params = {}) ->
      @active_season = App.request 'get:active_season', @get_season_id(params)      
      params['ratingable_id'] = id
      @listenToOnce @active_season, 'sync', =>
        @show(params, "#{window.olymp_root}/api/user_olympiad_ratings", "olympiads/#{id}/user_ratings", 'Ученик')

    show: (params, url_link, root_name, text) ->
      App.addRegions
        rating_region: '.container_rating'

      @initialize_filter(params, root_name)        
      @params = _.clone(params) 

      @collection = App.request 'get:ratings', @params, url_link

      @listenToOnce @collection, 'sync', ->
        rating_view = new Rating.ContentView
          collection: @collection
          validations: @params
          active_season: @active_season
          text: text

        App.getRegion('rating_region').show rating_view

    initialize_filter: (params, root_name) ->
      @filter or= new Stoege.Views.LibraryFilter
        collection_seasons: new Filters.Seasons
        collection_regions: new Stoege.Collections.Regions
        collection_cities: new Stoege.Collections.Cities(root: window.olymp_root)
        collection_grades: new Stoege.Collections.Grades
        root_name: root_name
        params: params

    get_season_id: (params) ->
      if params.season_id?
        params.season_id
      else
        if app_options.active_season_id 
          app_options.active_season_id 
        else
          app_options.newest_season_id

    get_url_link: (points_link, users_link) ->
      if @active_season.get('state') is 'results_opened'
        points_link
      else
        users_link
