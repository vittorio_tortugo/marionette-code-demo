@OlympiadsApp.module 'Olymp.TaskPreview', (TaskPreview, App, Backbone, Marionette, $, _) ->
  TasksCollection = App.request 'admin:get:tasks_collection'
  TasksIndexView = App.request 'admin:get:tasks_index_view'

  class TaskPreview.Controller extends App.Tasks.Controller
    show: (task_id) ->
      deferred = $.Deferred()
      deferred.done =>
        @tasks_collection.toggle_active_by(task_id)
      if @tasks_collection
        deferred.resolve()
      else
        @tasks_collection = new TasksCollection olympiad_id: null
        @tasks_collection.url = "#{window.olymp_root}/api/task_previews/#{task_id}"
        new TasksIndexView collection: @tasks_collection
        @tasks_collection.fetch().success ->
          deferred.resolve()

  class TaskPreview.Router extends Marionette.AppRouter
    controller: new TaskPreview.Controller
    appRoutes:
      'admin/olymp/tasks/:task_id/preview': 'show'

  App.on 'start', ->
    new TaskPreview.Router
