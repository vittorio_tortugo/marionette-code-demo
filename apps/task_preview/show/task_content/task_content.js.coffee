@OlympiadsApp.module 'Olymp.TaskPreview.Show.TaskContent', (TaskContent, App, Backbone, Marionette) ->

  class TaskContent.TaskContentView extends App.Tasks.Show.TaskContent.TaskContentView

  App.reqres.setHandler 'get:task_content_view', ->
    TaskContent.TaskContentView
