@OlympiadsApp.module 'Olymp.TaskPreview.Show.List', (TasksList, App, Backbone, Marionette) ->
  class TasksList.TaskView extends App.Tasks.Show.List.TaskView
    modelEvents: {}

  class TasksList.TasksListView extends App.Tasks.Show.List.TasksListView
    className: 'lesson_box block_rounded_shadow'
    childView: TasksList.TaskView

  App.reqres.setHandler 'admin:get:tasks_list_view', ->
    TasksList.TasksListView
