@OlympiadsApp.module 'Olymp.TaskPreview.Show', (Show, App, Backbone, Marionette) ->

  TaskContentView = App.request 'get:task_content_view'

  class Show.ShowView extends App.Tasks.Show.ShowView
    initialize: ({@collection}) ->
      console.log 'qww'
      ListView = App.request 'admin:get:tasks_list_view'
      list_view = new ListView collection: @collection
      @tasks_list_region.show list_view

      App.reqres.setHandler 'select_active_task', (list_task_model) =>
        content_model = App.request 'get:task_content_preview', list_task_model.get('olympiad_id'), list_task_model.get('id')
        
        @listenToOnce content_model, 'sync', =>
          task_content_view = new TaskContentView model: content_model, list_model: list_task_model

          @task_content_region.show task_content_view

  App.reqres.setHandler 'admin:get:tasks_index_view', ->
    Show.ShowView
