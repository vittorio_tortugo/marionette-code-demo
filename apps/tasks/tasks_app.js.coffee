@OlympiadsApp.module 'Tasks', (Tasks, App, Backbone, Marionette) ->
  TasksCollection = App.request 'get:tasks_collection'
  TasksIndexView = App.request 'get:tasks_index_view'

  class Tasks.Controller extends Marionette.Object
    show: (olympiad_id, id) ->
      deferred = $.Deferred()
      deferred.done =>
        @tasks_collection.toggle_active_by(id)

      if @tasks_collection
        deferred.resolve()
      else
        @tasks_collection = new TasksCollection olympiad_id: olympiad_id
        new TasksIndexView collection: @tasks_collection
        @tasks_collection.fetch().success ->
          deferred.resolve()

  class Tasks.Router extends Marionette.AppRouter
    controller: new Tasks.Controller
    appRoutes:
      'olympiads/:olympiad_id/tasks': 'show'
      'olympiads/:olympiad_id/tasks/:id': 'show'

  App.on 'start', ->
    new Tasks.Router
