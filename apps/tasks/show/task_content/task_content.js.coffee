@OlympiadsApp.module 'Tasks.Show.TaskContent', (TaskContent, App, Backbone, Marionette) ->

  class TaskContent.QuestionView extends Marionette.ItemView
    template: 'tasks/show/task_content/templates/question'
    tagName: 'li'

  class TaskContent.QuestionsView extends Marionette.CollectionView
    tagName: 'ul'
    childView: TaskContent.QuestionView

  class TaskContent.TaskContentView extends Marionette.LayoutView
    template: 'tasks/show/task_content/templates/task_content'
    regions: 
      questions: '.questions_region'
      answers: '.task_questions_block'
      
    behaviors:
      MathJaxBehavior: {}

    events:
      'click .blue_button': 'submit_task'
      'click .toggle_header, .toggle_arrow' : 'toggle_content'

    initialize: ({@list_model}) ->

    onRender: ->
      $('form').attr('action', "#{window.olymp_root}/api/olympiads/#{@list_model.get('olympiad_id')}/user_answers/#{@list_model.user_answer_id}")
      @render_questions(@model.questions_collection)
      @render_answers(@model.answers_collection)

    render_questions: (collection) ->
      questions_view = new TaskContent.QuestionsView collection: collection

      @getRegion('questions').show questions_view

    render_answers: (collection) ->
      answers_view = App.request 'get:task_answers', collection
      
      @getRegion('answers').show answers_view

    submit_task: (e) ->
      #будет доделываться
      $('form').submit ->
        error: (data, status, xhr) =>
          console.log 'error'
        success: (data, status, xhr) =>
          console.log 'success'

    toggle_content: (e)=>
      $target = $(e.target)
      $arrow = $target.closest('.toggle_element').find('.toggle_arrow')

      $target.closest('.toggle_element').children('.toggle_content').slideToggle(300)
      $arrow.toggleClass("down")


  App.reqres.setHandler 'get:task_content_view', ->
    TaskContent.TaskContentView
