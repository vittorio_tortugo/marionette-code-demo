@OlympiadsApp.module "Tasks.Show.TaskAnswers", (TaskAnswers, App, Backbone, Marionette, $, _) ->

  App.reqres.setHandler 'get:task_answers', (collection) ->
    type = collection.at(0).get('params_set')[0].answer_type

    if type is 'radio_button'
      new TaskAnswers.Radio.CollectionView collection: collection
    else if type is 'checkbox'
      new TaskAnswers.Checkbox.CollectionView collection: collection
    else if type is 'text'
      new TaskAnswers.Text.CollectionView collection: collection
    else if type is 'link'
      new TaskAnswers.Links.ContentView collection: collection