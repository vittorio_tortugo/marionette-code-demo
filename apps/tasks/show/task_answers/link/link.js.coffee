@OlympiadsApp.module "Tasks.Show.TaskAnswers.Links", (Links, App, Backbone, Marionette, $, _) ->

  class Links.SecondItemView extends Marionette.ItemView
    template: 'tasks/show/task_answers/link/templates/second_item'
    behaviors:
      UnwrapBehavior: {}

  class Links.SecondCollectionView extends Marionette.CollectionView      
    tagName: 'ul'
    className: 'links_collection_second'
    childView: Links.SecondItemView

  class Links.FirstItemView extends Marionette.ItemView
    template: 'tasks/show/task_answers/link/templates/first_item'
    behaviors:
      UnwrapBehavior: {}

  class Links.FirstCollectionView extends Marionette.CollectionView      
    tagName: 'ul'
    className: 'links_collection_first'
    childView: Links.FirstItemView

  class Links.ContentView extends Marionette.LayoutView
    template: 'tasks/show/task_answers/link/templates/content'
    className: 'task_questions_links'

    attributes:
      'data-type': 'links'

    regions:
      first_links: '.first_links_region'
      second_links: '.second_links_region'

    onRender: ->
      @render_first_links()
      @render_second_links()

    onShow: ->
      _.delay (=>
        new Stoege.Views.LinksQuestionView()
      ), 200

    render_first_links: ->
      first_links_collection = new Links.FirstCollectionView collection: @collection

      @getRegion('first_links').show first_links_collection

    render_second_links: ->
      second_links_collection = new Links.SecondCollectionView collection: @collection

      @getRegion('second_links').show second_links_collection
