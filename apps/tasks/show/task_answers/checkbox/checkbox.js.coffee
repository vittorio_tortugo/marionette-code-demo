@OlympiadsApp.module "Tasks.Show.TaskAnswers.Checkbox", (Checkbox, App, Backbone, Marionette, $, _) ->

  class Checkbox.ItemView extends Marionette.ItemView
    template: 'tasks/show/task_answers/checkbox/templates/item'

    events:
      'click label': 'set_button_state' 

    initialize: ({@task_id})->
      console.log @model

    templateHelpers: ->
      task_id: @task_id

    set_button_state: (e) ->
      $label = $(e.target)
      return if $label.parent('.task_questions_checkbox_answer').hasClass('solved')

      if $label.hasClass('active')
        # снимаю выделение с элемента
        $label.removeClass('active')
        $label.siblings('input[type="checkbox"]').prop('checked', false)
      else
        # выделяю элемент
        $label.addClass('active')
        $label.siblings('input[type="checkbox"]').prop('checked', true)

  class Checkbox.CollectionView extends Marionette.CollectionView
    className: 'task_questions_checkbox'
    childView: Checkbox.ItemView
    attributes: ->
      'data-type': 'checkbox'

    initialize: ({@task_id})->

    childViewOptions: ->
      task_id: @task_id
