@OlympiadsApp.module "Tasks.Show.TaskAnswers.Radio", (Radio, App, Backbone, Marionette, $, _) ->

  class Radio.ItemView extends Marionette.ItemView
    template: 'tasks/show/task_answers/radio/templates/item'

    events:
      'click label': 'set_button_state' 

    initialize: ({@task_id})->

    templateHelpers: ->
      task_id: @task_id

    set_button_state: (e) ->
      $label = $(e.target)
      return if $label.hasClass('active') or $label.parent('.task_questions_radio_answer').hasClass('solved')

      # снимаю выделение с предыдущего элемента
      $active_label = $label.closest('.task_questions_radio').find('label.active')
      $active_label.removeClass('active')
      $active_label.siblings('input[type="radio"]').prop('checked', false)
      # выделяю новый
      $label.addClass('active')
      $label.siblings('input[type="radio"]').prop('checked', true)

  class Radio.CollectionView extends Marionette.CollectionView
    className: 'task_questions_radio'
    childView: Radio.ItemView 
    attributes: ->
      'data-type': 'radio'

    initialize: ({@task_id})->

    childViewOptions: ->
      task_id: @task_id
