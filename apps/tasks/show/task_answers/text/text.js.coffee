@OlympiadsApp.module "Tasks.Show.TaskAnswers.Text", (Text, App, Backbone, Marionette, $, _) ->

  class Text.ItemView extends Marionette.ItemView
    template: 'tasks/show/task_answers/text/templates/item'

    initialize: ({@task_id})->

    templateHelpers: ->
      task_id: @task_id

  class Text.CollectionView extends Marionette.CollectionView
    className: 'task_questions_text'
    childView: Text.ItemView
    attributes: ->
      'data-type': @collection.at(0).get('answer_type')

    initialize: ({@task_id})->

    childViewOptions: ->
      task_id: @task_id


