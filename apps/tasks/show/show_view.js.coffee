@OlympiadsApp.module 'Tasks.Show', (TasksShow, App, Backbone, Marionette) ->

  class TasksShow.ShowView extends Marionette.LayoutView
    el: '.content'
    regions:
      tasks_list_region: '.lesson_box_wrapper'
      task_content_region: '.task_content_wrapper'

    initialize: ({@collection}) ->
      ListView = App.request 'get:tasks_list_view'
      TaskContentView = App.request 'get:task_content_view'

      list_view = new ListView collection: @collection
      @tasks_list_region.show list_view

      App.reqres.setHandler 'select_active_task', (list_task_model) =>
        content_model = App.request 'get:task_content', list_task_model.get('olympiad_id'), list_task_model.get('id')
        
        @listenToOnce content_model, 'sync', =>
          task_content_view = new TaskContentView model: content_model, list_model: list_task_model

          @task_content_region.show task_content_view

  App.reqres.setHandler 'get:tasks_index_view', ->
    TasksShow.ShowView
