@OlympiadsApp.module 'Tasks.Show.List', (TasksList, App, Backbone, Marionette) ->
  class TasksList.TaskView extends Marionette.ItemView
    template: 'tasks/show/list/templates/item'
    modelEvents:
      'change:active': 'render'

  class TasksList.TasksListView extends Marionette.CollectionView
    className: 'lesson_box block_rounded_shadow'
    childView: TasksList.TaskView

  App.reqres.setHandler 'get:tasks_list_view', ->
    TasksList.TasksListView
