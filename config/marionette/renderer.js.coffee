Backbone.Marionette.Renderer.render = (template_url, data) ->
  template_url = "olymp/apps/#{template_url}"
  template = JST[template_url]

  throw "Template #{template_url} not found" unless template
  template(data)