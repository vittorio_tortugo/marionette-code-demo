@OlympiadsApp.module "Behaviors", (Behaviors, App, Backbone, Marionette, $, _) ->
  class Behaviors.MathJaxBehavior extends Marionette.Behavior
    
    onShow: ->
      _.defer =>
        MathJax.Hub.Queue(
          ["Typeset", MathJax.Hub, 'mathjax_block']
          )


  window.Behaviors.MathJaxBehavior = Behaviors.MathJaxBehavior