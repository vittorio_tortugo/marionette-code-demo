@OlympiadsApp.module "Behaviors", (Behaviors, App, Backbone, Marionette, $, _) ->
  class Behaviors.UnwrapBehavior extends Marionette.Behavior

    onShow: ->
      @$el = @$el.children()
      @$el.unwrap()
      @view.setElement(@$el)

  window.Behaviors.UnwrapBehavior = Behaviors.UnwrapBehavior