@OlympiadsApp.module 'Entities.Filters', (Filters, App, Backbone) ->

  class Filters.Seasons extends Stoege.Collections.FilterableCollection
    model: Stoege.Models.FilterableModel

    initialize: ->
      for season in $('#filter_seasons > option')
        @add
          id: $(season).attr('value')
          selected: $(season).attr('selected')
          text: $(season).text()

  API = 
    get_active_season: (season_id) ->
      season = new Stoege.Models.FilterableModel

      season.fetch
        url: "#{window.olymp_root}/api/season_states/#{season_id}"

      season


  App.reqres.setHandler 'get:active_season', API.get_active_season
