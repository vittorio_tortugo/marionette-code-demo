@OlympiadsApp.module 'Entities.Olympiads', (Olympiads, App, Backbone) ->
  finished_timer_end_data =
    countdown_days: '00'
    countdown_hours: '00'
    countdown_minutes: '00'

  class Olympiads.OlympiadModel extends Backbone.Model
    urlRoot: ->
      "#{window.olymp_root}/api/olympiads/"
    validate: (key, value) ->
      value = parseInt value
      switch key
        when 'grade_id'
          _.where(@get('grades'), id: value).length > 0
        when 'discipline_id'
          @get('discipline').id is value
        when 'season_id'
          @get('season_id') is value
        else
          false

    start_timer: ->
      if @get('state') in ['published', 'active']
        @update_timer_data()
        @initialize_timer_updating()
      else
        @set 'timer_end_data', finished_timer_end_data

    get_end_time_from_now: ->
      end_time = if @get('state') is 'published' then @get('starts_at') else @get('finishes_at')
      Stoege.Services.Countdown.get_countdown_data_from_now(end_time)

    initialize_timer_updating: ->
      @update_interval_id or= setInterval(@update_timer_data, 30 * 1000)

    update_timer_data: =>
      timer_end_data = @get_end_time_from_now()
      if timer_end_data.countdown_time_diff > 0
        @set 'timer_end_data', timer_end_data
      else if @change_state()
        @update_timer_data()
      else
        @set 'timer_end_data', finished_timer_end_data
        clearInterval @update_interval_id

    change_state: ->
      current_state = @get 'state'
      
      new_state = switch current_state
        when 'published'  then 'active'
        when 'active'     then 'finished'

      return false unless new_state
      @set 'state', new_state

    subscribe_user: ->
      @set('users_count', @get('users_count') + 1)
      @set 'user_subscribed', true

  class Olympiads.Collection extends Backbone.Collection
    model: Olympiads.OlympiadModel
    url: ->
      "#{window.olymp_root}/api/olympiads/"

    get_count_of_valid_elements: (validations) ->
      @filter( (child) => @try_validation_for(child, validations) ).length

    try_validation_for: (child, validations) ->
      for key, value of validations
        return false unless child.validate(key, value)
      return true


  API = 
    get_olympiads: ->
      olympiads = new Olympiads.Collection

      olympiads.fetch()

      olympiads

    get_olympiad: (id) ->
      olympiad = new Olympiads.OlympiadModel id: id

      olympiad.fetch()

      olympiad

  App.reqres.setHandler 'get:olympiads', API.get_olympiads  
  App.reqres.setHandler 'get:olympiad', API.get_olympiad
