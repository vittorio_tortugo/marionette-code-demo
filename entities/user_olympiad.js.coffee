@OlympiadsApp.module 'Entities.UserOlympiads', (UserOlympiads, App, Backbone) ->
  class UserOlympiads.OlympiadModel extends Backbone.Model
    url: ->
      "#{window.olymp_root}/api/user_olympiads"

  API = 
    get_user_olympiad: ->
      user_olympiad = new UserOlympiads.OlympiadModel
      user_olympiad.fetch()
      user_olympiad

  App.reqres.setHandler 'get:user_olympiad', API.get_user_olympiad