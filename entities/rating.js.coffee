@OlympiadsApp.module 'Entities.Rating', (Rating, App, Backbone) ->
  class Rating.RatingModel extends Backbone.Model    

  class Rating.RatingCollection extends Backbone.PageableCollection
    model: Rating.RatingModel

    mode: 'infinite'

    url: -> @url_link

    state:
      firstPage: 1

    queryParams:
      totalPages: null
      totalRecords: null
      pageSize: 'count'

    initialize: ({@url_link}) ->

    parseLinks: (resp, options) ->
      next: @url_link

  API = 
    get_ratings: (validations, url_link) ->
      rating = new Rating.RatingCollection
        url_link: url_link

      rating.fetch
        data: validations

      rating

  App.reqres.setHandler 'get:ratings', API.get_ratings