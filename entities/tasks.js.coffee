@OlympiadsApp.module 'Entities.Tasks', (Tasks, App, Backbone) ->
  class Tasks.TaskContentModel extends Backbone.Model
    url: ->
      "#{window.olymp_root}/api/olympiads/#{@olympiad_id}/tasks/#{@task_id}"

    initialize: ({@olympiad_id, @task_id}) ->
      @on 'sync', ->
        @set_answers()
        @set_questions()

    set_questions: ->
      @questions_collection = new Backbone.Collection(@get('questions'))

    set_answers: ->
      @answers_collection = new Backbone.Collection(@get('answers'))

  class Tasks.ListTaskModel extends Backbone.Model

  class Tasks.ListTasksCollection extends Backbone.Collection
    model: Tasks.ListTaskModel
    comparator: 'position'

    url: ->
      "#{window.olymp_root}/api/olympiads/#{@olympiad_id}/tasks"

    initialize: ({@olympiad_id}) ->
      return @

    toggle_active_by: (id) ->
      active_item = @get_active()
      active_item.set('active', false) if active_item

      if id
        new_active_item = @make_active_by(id)
      else
        new_active_item = @make_first_active()
      @fire_select_active_task(new_active_item)

    get_active: ->
      @findWhere('active': true)

    make_active_by: (id) ->
      @set_item_active(@get id)

    make_first_active: ->
      @set_item_active(@first())

    set_item_active: (item) ->
      return unless item
      item.set('active', true) if item

    fire_select_active_task: (item) ->
      return unless item
      App.request 'select_active_task', item

  App.reqres.setHandler 'get:tasks_collection', ->
    Tasks.ListTasksCollection

  App.reqres.setHandler 'admin:get:tasks_collection', ->
    Tasks.ListTasksCollection
    
  App.reqres.setHandler 'get:task_content', (olympiad_id, task_id) ->
    task_content = new Tasks.TaskContentModel olympiad_id: olympiad_id, task_id: task_id
    task_content.fetch()
    task_content

  App.reqres.setHandler 'get:task_content_preview', (olympiad_id, task_id) ->
    task_content = new Tasks.TaskContentModel olympiad_id: olympiad_id, task_id: task_id
    task_content.url = "#{window.olymp_root}/api/task_previews/#{task_id}"
    task_content.fetch()
    task_content

