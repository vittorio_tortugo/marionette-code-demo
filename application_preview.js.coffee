#= require_self
#= require_tree ./config
#= require_tree ./behaviors
#= require_tree ./entities
#= require_tree ./apps/tasks
#= require_tree ./apps/task_preview

@OlympiadsApp = new Marionette.Application

$ =>
  window.olymp_root = '/o'
  new Stoege.Views.GlobalView
  @OlympiadsApp.start()
  Backbone.history.start pushState: true
